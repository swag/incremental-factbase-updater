# Incremental Updater Prototype

This repository contains a prototype for Updating a Neo4j Factbase (derived from soruce code) using file-based diffs and Neo4j Native API.

## Dependencies

To *build* the prototype, make sure you have the following dependcies installed.

### Apache Maven

Make sure you have Apache Maven installed, at least version `3.3.9`. For Debian-based systems, you can install Maven using your package manager:
```
$ sudo apt-get update
$ sudo apt-get install mave
```

You can check the version of Maven you have installed using:
```
$ mvn -version
```

### Java 8

Make sure you have the Java 8 SDK and the Java 8 runtime installed. For Debian-based systems, we recommend installing OpenJDK 8:
```
$ sudo apt-get update
$ sudo apt-get install default-jdk
```

### Neo4j Server (Community Edition)

Neo4j version `3.5.X` is required. You can either install it using your OS' package manager, or install it from source. More information can be found here: https://neo4j.com/docs/operations-manual/current/installation/

## Build

Once all dependencies have been installed, you can build the prototype using the following commands:
```
$ cd updater
$ mvn package
```
This will produce two JAR files: `u[dater/target/incremental-1.0.0.jar` and `updater/target/incremental-1.0.0-jar-with-dependencies.jar`. The latter contains all program dependecies packaged into one JAR, while the former does not.