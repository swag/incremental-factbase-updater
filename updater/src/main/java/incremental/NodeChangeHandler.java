package incremental;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import incremental.listeners.NodeObserver;
import incremental.listeners.SubjectData;
import org.neo4j.graphdb.*;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class NodeChangeHandler extends EntityChangeHandler
{
    Label BaseType = Label.label("BaseFact");

    public void handleEntityAdded(JsonArray arr, GraphDatabaseService db)
    {
        Iterator it =  arr.iterator();
        while(it.hasNext())
        {
            JsonObject node = (JsonObject) it.next();
            Label nodeType = Label.label(node.get("type").getAsString());
            Node foundNode = db.findNode(BaseType,"id", node.get("id").getAsString());
            if(foundNode == null)
            {
                foundNode = db.createNode(nodeType, BaseType);
                foundNode.setProperty("id", node.get("id").getAsString());
                System.out.println("Add a new node: " + node.get("id").getAsString() + ":" + nodeType.name());
                SubjectData sd = new SubjectData();
                sd.add("time", System.currentTimeMillis());
                sd.add("nodeID", node.get("id").getAsString());
                sd.add("nodeType", node.get("type").getAsString());
                sd.add("type", "add");
                notifyObserver("node", sd);
            }
            else if(!foundNode.hasLabel(nodeType))
            {
                foundNode.addLabel(nodeType);
                SubjectData sd = new SubjectData();
                sd.add("time", System.currentTimeMillis());
                sd.add("nodeID", node.get("id").getAsString());
                sd.add("nodeType", node.get("type").getAsString());
                sd.add("type", "add");
                notifyObserver("node", sd);
            }
        }
    }

    public void handleEntityAttrAdded(JsonArray arr, GraphDatabaseService db)
    {
        Iterator it = arr.iterator();
        while(it.hasNext())
        {
            JsonObject nodeAttr = (JsonObject) it.next();
            SubjectData sd = new SubjectData();
            Label nodeType = Label.label(nodeAttr.get("type").getAsString());
            Node foundNode = db.findNode(BaseType,"id", nodeAttr.get("id").getAsString());
            if(foundNode != null)
            {
                sd.add("type", "mod");
                sd.add("time", System.currentTimeMillis());
                sd.add("nodeID", nodeAttr.get("id").getAsString());
                sd.add("nodeType", nodeAttr.get("type").getAsString());
                applyEntityAttrAdded(foundNode, nodeAttr, sd);
                notifyObserver("node", sd);
            }
        }
    }

    public void handleEntityRemoved(JsonArray arr, GraphDatabaseService db)
    {
        Iterator it = arr.iterator();
        while(it.hasNext())
        {
            JsonObject node = (JsonObject) it.next();
//            Label nodeType = Label.label(node.get("type").getAsString());
            Node foundNode = db.findNode(BaseType, "id", node.get("id").getAsString());
            if(foundNode != null)
            {
                SubjectData nodeData = new SubjectData();
                int numInstances = ((Number) foundNode.getProperty("instances")).intValue();
                System.out.println(numInstances);
                if(numInstances == 0)
                {
                    if(foundNode.hasRelationship())
                    {
                        for(Relationship r : foundNode.getRelationships())
                        {
                            SubjectData sd = new SubjectData();
                            sd.add("time", System.currentTimeMillis());
                            sd.add("edgeType", r.getType().name());
                            sd.add("src", (String) r.getStartNode().getProperty("id"));
                            sd.add("dst", (String) r.getEndNode().getProperty("id"));
                            sd.add("type", "remove");
                            r.delete();
                            notifyObserver("edge", sd);
                        }
                    }

                    nodeData.add("time", System.currentTimeMillis());
                    nodeData.add("nodeID", node.get("id").getAsString());
                    nodeData.add("nodeType", node.get("type").getAsString());
                    nodeData.add("type", "remove");
                    foundNode.delete();
                    notifyObserver("node", nodeData);
                }

            }
        }
    }

    public void handleEntityAttrRemoved(JsonArray arr, GraphDatabaseService db)
    {
        Iterator it = arr.iterator();
        while(it.hasNext())
        {
            JsonObject nodeAttr = (JsonObject) it.next();
            SubjectData sd = new SubjectData();
            sd.add("type", "mod");
            sd.add("time", System.currentTimeMillis());
            sd.add("nodeID", nodeAttr.get("id").getAsString());
            sd.add("nodeType", nodeAttr.get("type").getAsString());
//            Label nodeType = Label.label(nodeAttr.get("type").getAsString());
            Node foundNode = db.findNode(BaseType,"id", nodeAttr.get("id").getAsString());
            if(foundNode != null)
            {
                System.out.println("Checking " + sd.get("nodeID"));
                applyEntityAttrRemoved(foundNode, nodeAttr, sd);
                notifyObserver("node", sd);
            }
        }
    }

    public void handleEntityAttrChange(JsonArray arr, GraphDatabaseService db)
    {
        Iterator it = arr.iterator();
        while(it.hasNext())
        {
            JsonObject nodeAttrChange = (JsonObject) it.next();
//            Label nodeType = Label.label(nodeAttrChange.get("type").getAsString());
            SubjectData sd = new SubjectData();
            sd.add("type", "mod");
            sd.add("time", System.currentTimeMillis());
            sd.add("nodeID", nodeAttrChange.get("id").getAsString());
            sd.add("nodeType", nodeAttrChange.get("type").getAsString());
            Node foundNode = db.findNode(BaseType,"id", nodeAttrChange.get("id").getAsString());
            if(foundNode != null)
            {
                JsonElement singleAttrs = nodeAttrChange.get("singleAttrs");
                if(singleAttrs != null)
                {
                    applySingleAttrChange(singleAttrs.getAsJsonObject(), foundNode, sd);
                }
                JsonElement multiAttrs = nodeAttrChange.get("multiAttrs");
                if(multiAttrs != null)
                {
                    applyMultiAttrChange(multiAttrs.getAsJsonObject(), foundNode, sd);
                }
                notifyObserver("node", sd);
            }
        }
    }

//    @Override
//    public void applyEntityAttrRemoved(Entity foundNode, JsonObject nodeAttr, SubjectData sd)
//    {
//        Node systemNode = (Node) foundNode;
//        if(EntityComparator.isEqual(systemNode, nodeAttr))
//        {
//            systemNode.delete();
//            System.out.println("Deleted Node");
//            sd.set("type", "remove");
//        }
//        else
//        {
//            super.applyEntityAttrRemoved(systemNode, nodeAttr, sd);
//        }
//    }

}
