package incremental;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import incremental.listeners.SubjectData;
import org.neo4j.graphdb.*;

import java.util.Iterator;

public class EdgeChangeHandler extends EntityChangeHandler
{

    //TODO: this should be encoded in the diff
    Label BaseType = Label.label("BaseFact");

    @Override
    public void handleEntityAdded(JsonArray arr, GraphDatabaseService db)
    {
        Iterator it =  arr.iterator();
        while(it.hasNext())
        {
            JsonObject edge = (JsonObject) it.next();
            RelationshipType edgeType = RelationshipType.withName(edge.get("type").getAsString());
            Node srcNode = db.findNode(BaseType, "id", edge.get("srcID").getAsString());
            if(srcNode == null)
            {
                srcNode = db.createNode(BaseType);
                srcNode.setProperty("id", edge.get("srcID").getAsString());
                System.out.println("Add a new node: " + edge.get("srcID").getAsString());
                SubjectData sd = new SubjectData();
                sd.add("time", System.currentTimeMillis());
                sd.add("nodeID", edge.get("srcID").getAsString());
                sd.add("type", "add");
                notifyObserver("node", sd);
            }
            Node dstNode = db.findNode(BaseType, "id", edge.get("dstID").getAsString());
            if(dstNode == null)
            {
                dstNode = db.createNode(BaseType);
                dstNode.setProperty("id", edge.get("dstID").getAsString());
                System.out.println("Add a new node: " + edge.get("dstID").getAsString());
                SubjectData sd = new SubjectData();
                sd.add("time", System.currentTimeMillis());
                sd.add("nodeID", edge.get("dstID").getAsString());
                sd.add("type", "add");
                notifyObserver("node", sd);
            }
            boolean relExists = false;
            for(Relationship r : srcNode.getRelationships(Direction.OUTGOING, edgeType))
            {
                if(r.getEndNode().equals(dstNode))
                {
                    relExists = true;
                    break;
                }
            }
            if(!relExists)
            {
                System.out.println("Add a new edge! : " + edgeType.name() + " " + edge.get("srcID").getAsString() + " " + edge.get("dstID").getAsString());
                srcNode.createRelationshipTo(dstNode, edgeType);
                SubjectData sd = new SubjectData();
                sd.add("time", System.currentTimeMillis());
                sd.add("edgeType", edgeType.name());
                sd.add("type", "add");
                sd.add("src", edge.get("srcID").getAsString());
                sd.add("dst", edge.get("dstID").getAsString());
                notifyObserver("edge", sd);
            }
        }
    }

    @Override
    public void handleEntityAttrAdded(JsonArray arr, GraphDatabaseService db)
    {
        Iterator it = arr.iterator();
        while(it.hasNext())
        {
            JsonObject edgeAttr = (JsonObject) it.next();
            RelationshipType edgeType = RelationshipType.withName(edgeAttr.get("type").getAsString());
            Node srcNode = db.findNode(BaseType, "id", edgeAttr.get("srcID").getAsString());
            Node dstNode = db.findNode(BaseType, "id", edgeAttr.get("dstID").getAsString());
            SubjectData sd = new SubjectData();
            sd.add("time", System.currentTimeMillis());
            sd.add("edgeType", edgeType.name());
            sd.add("type", "mod");
            sd.add("src", edgeAttr.get("srcID").getAsString());
            sd.add("dst", edgeAttr.get("dstID").getAsString());
            if(srcNode != null && dstNode != null)
            {
                for (Relationship r : srcNode.getRelationships(edgeType, Direction.OUTGOING))
                {
                    if(r.getEndNode().equals(dstNode))
                    {
                        applyEntityAttrAdded(r, edgeAttr, sd);
                        notifyObserver("edge", sd);
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void handleEntityRemoved(JsonArray arr, GraphDatabaseService db)
    {
        Iterator it = arr.iterator();
        while(it.hasNext())
        {
            SubjectData sd = new SubjectData();
            JsonObject edgeRemoved = (JsonObject) it.next();
            System.out.println(edgeRemoved);
            RelationshipType edgeType = RelationshipType.withName(edgeRemoved.get("type").getAsString());
            Node srcNode = db.findNode(BaseType, "id", edgeRemoved.get("srcID").getAsString());
            Node dstNode = db.findNode(BaseType, "id", edgeRemoved.get("dstID").getAsString());
            if(srcNode != null && dstNode != null)
            {
                for (Relationship r : srcNode.getRelationships(edgeType, Direction.OUTGOING)) {
                    if (r.getEndNode().equals(dstNode))
                    {
                        if(r.hasProperty("instances"))
                        {
                            sd.add("time", System.currentTimeMillis());
                            sd.add("edgeType", edgeType.name());
                            sd.add("src", edgeRemoved.get("srcID").getAsString());
                            sd.add("dst", edgeRemoved.get("dstID").getAsString());
                            int numInstances = ((Number) (r.getProperty("instances"))).intValue();
                            if (numInstances == 0)
                            {
                                sd.add("type", "remove");
                                r.delete();
                                notifyObserver("edge", sd);
                            }

                        }
                        break;
                    }
                }
            }

        }
    }

    @Override
    public void handleEntityAttrRemoved(JsonArray arr, GraphDatabaseService db)
    {
        Iterator it = arr.iterator();
        while(it.hasNext())
        {
            JsonObject edgeAttrRemoved = (JsonObject) it.next();
            System.out.println(edgeAttrRemoved);
            RelationshipType edgeType = RelationshipType.withName(edgeAttrRemoved.get("type").getAsString());
            Node srcNode = db.findNode(BaseType, "id", edgeAttrRemoved.get("srcID").getAsString());
            Node dstNode = db.findNode(BaseType, "id", edgeAttrRemoved.get("dstID").getAsString());
            SubjectData sd = new SubjectData();
            sd.add("time", System.currentTimeMillis());
            sd.add("edgeType", edgeType.name());
            sd.add("type", "mod");
            sd.add("src", edgeAttrRemoved.get("srcID").getAsString());
            sd.add("dst", edgeAttrRemoved.get("dstID").getAsString());
            if(srcNode != null && dstNode != null)
            {
                for (Relationship r : srcNode.getRelationships(edgeType, Direction.OUTGOING)) {
                    if (r.getEndNode().equals(dstNode)) {
                        System.out.println("Checking " + edgeType.name() + " " + sd.get("src") + " " + sd.get("dst"));
                        applyEntityAttrRemoved(r, edgeAttrRemoved, sd);
                        notifyObserver("edge", sd);
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void handleEntityAttrChange(JsonArray arr, GraphDatabaseService db)
    {
        Iterator it = arr.iterator();
        while(it.hasNext())
        {
            JsonObject edgeAttrChange = (JsonObject) it.next();
            System.out.println(edgeAttrChange);
            RelationshipType edgeType = RelationshipType.withName(edgeAttrChange.get("type").getAsString());
            Node srcNode = db.findNode(BaseType, "id", edgeAttrChange.get("srcID").getAsString());
            Node dstNode = db.findNode(BaseType, "id", edgeAttrChange.get("dstID").getAsString());
            SubjectData sd = new SubjectData();
            sd.add("time", System.currentTimeMillis());
            sd.add("edgeType", edgeType.name());
            sd.add("type", "mod");
            sd.add("src", edgeAttrChange.get("srcID").getAsString());
            sd.add("dst", edgeAttrChange.get("dstID").getAsString());
            if(srcNode != null && dstNode != null)
            {
                for (Relationship r : srcNode.getRelationships(edgeType, Direction.OUTGOING)) {
                    if (r.getEndNode().equals(dstNode)) {
                        //Do Stuff
                        JsonElement singleAttrs = edgeAttrChange.get("singleAttrs");
                        if (singleAttrs != null) {
                            applySingleAttrChange(singleAttrs.getAsJsonObject(), r, sd);
                        }
                        JsonElement multiAttrs = edgeAttrChange.get("multiAttrs");
                        if (multiAttrs != null) {
                            applyMultiAttrChange(multiAttrs.getAsJsonObject(), r, sd);
                        }
                        notifyObserver("edge", sd);
                        break;
                    }
                }
            }

        }
    }

//    @Override
//    public void applyEntityAttrRemoved(Entity foundNode, JsonObject edgeAttr, SubjectData sd)
//    {
//        Relationship systemEdge = (Relationship) foundNode;
//        if(EntityComparator.isEqual(systemEdge, edgeAttr))
//        {
//            systemEdge.delete();
//            System.out.println("Deleted Edge");
//            sd.set("type", "remove");
//        }
//        else
//        {
//            super.applyEntityAttrRemoved(systemEdge, edgeAttr, sd);
//        }
//    }
}
