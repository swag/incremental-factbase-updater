package incremental;

public interface MergeRule<G, T>
{
    void merge(G system, T local);

    void unMerge(G system, T local);

    void reMerge(G system, T local);
}
