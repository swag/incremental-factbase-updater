package incremental;

public interface FuncEquality
{
    boolean isEqual(Attribute a1, Attribute a2);
}
