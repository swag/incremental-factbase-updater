package incremental;

import com.google.gson.*;
import com.google.gson.stream.JsonReader;
import org.neo4j.graphdb.*;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.graphdb.schema.IndexDefinition;
import org.neo4j.graphdb.schema.Schema;


import java.io.*;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;


public class Link
{
    static GraphDatabaseService db;

    public static void main(String[] args)
    {

        String diffPath = args[0];
        String dbPath = args[1];
        db = new GraphDatabaseFactory().newEmbeddedDatabase(new File(dbPath));
        long start = System.currentTimeMillis();

        if(Files.isDirectory(Paths.get(diffPath)))
        {
            if (args.length == 3) {
                link(diffPath, Integer.parseInt(args[2]));
            } else {
                link(diffPath, -1);
            }
        }
        else
        {
            link(diffPath);
        }
        System.out.println("Time taken: " + (System.currentTimeMillis() - start));
    }

    public static void link(String diff_file)
    {
        File file = new File(diff_file);
        Transaction tx = db.beginTx();
        Schema schema = db.schema();
        if(!schema.getIndexes(Label.label("BaseFact")).iterator().hasNext()) {
            IndexDefinition nodeIndexDefinition = schema.indexFor(Label.label("BaseFact")).on("id").create();
        }
        if(!schema.getIndexes(Label.label("DerivedFact")).iterator().hasNext()) {
            IndexDefinition nodeIndexDefinition = schema.indexFor(Label.label("DerivedFact")).on("id").create();
        }
        if(!schema.getIndexes(Label.label("NodeDiff")).iterator().hasNext()) {
            IndexDefinition nodeDiffIndexDefinition = schema.indexFor(Label.label("NodeDiff")).on("nodeID").create();
        }
        if(!schema.getIndexes(Label.label("EdgeDiff")).iterator().hasNext()) {
            IndexDefinition edgeDiffIndexDefinition = schema.indexFor(Label.label("EdgeDiff")).on("edgeID").create();
        }
        tx.success();
        tx.close();

//        System.setOut(new PrintStream(new OutputStream() {
//            @Override
//            public void write(int arg0) throws IOException {}
//        }));

        try
        {
            JsonReader reader  = new JsonReader(new FileReader(file));
            //reader.setLenient(true);
            JsonObject obj = new JsonParser().parse(reader).getAsJsonObject();
            NodeChangeHandler nh = new NodeChangeHandler();
            EdgeChangeHandler eh = new EdgeChangeHandler();
            // Do Stuff with obj
            System.out.println(file.toString());
            Transaction tx2 = db.beginTx();
            handleRemovals(obj, nh, eh);
            handleChanges(obj, nh, eh);
            nh.recordNetChange(db);
            eh.recordNetChange(db);
            tx2.success();
            tx2.close();

            Transaction cleanUp = db.beginTx();
            ResourceIterator<Node> ri = db.findNodes(Label.label("BaseFact"));
            while(ri.hasNext())
            {
                Node n = ri.next();
                if(!n.hasProperty("instances"))
                {
                    for(Relationship r : n.getRelationships(Direction.OUTGOING))
                    {
                        String edgeID = r.getType().name() + " " + n.getProperty("id") + " " + r.getEndNode().getProperty("id");
                        Node edgeDiff = db.findNode(Label.label("EdgeDiff"), "edgeID", edgeID);
                        if(edgeDiff != null)
                        {
                            System.out.println("Deleting Virtual Edge Diff: " + edgeID);
                            edgeDiff.delete();
                        }
                        System.out.println("Deleting Virtual Edge: " + edgeID);
                        r.delete();
                    }
                    for(Relationship r : n.getRelationships(Direction.INCOMING))
                    {
                        String edgeID = r.getType().name() + " " + r.getStartNode().getProperty("id") + " " + n.getProperty("id");
                        Node edgeDiff = db.findNode(Label.label("EdgeDiff"), "edgeID", edgeID);
                        if(edgeDiff != null)
                        {
                            System.out.println("Deleting Virtual Edge Diff: " + edgeID);
                            edgeDiff.delete();
                        }
                        System.out.println("Deleting Virtual Edge: " + edgeID);
                        r.delete();
                    }
                    Node nodeDiff = db.findNode(Label.label("NodeDiff"), "nodeID", n.getProperty("id"));
                    nodeDiff.delete();
                    n.delete();
                }
            }
            cleanUp.success();
            cleanUp.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }



    public static void link(String root, int limit)
    {

        Path p = Paths.get(root);
        try
        {
            Transaction tx = db.beginTx();
            Schema schema = db.schema();
            if(!schema.getIndexes(Label.label("BaseFact")).iterator().hasNext()) {
                IndexDefinition nodeIndexDefinition = schema.indexFor(Label.label("BaseFact")).on("id").create();
            }
            if(!schema.getIndexes(Label.label("DerivedFact")).iterator().hasNext()) {
                IndexDefinition nodeIndexDefinition = schema.indexFor(Label.label("DerivedFact")).on("id").create();
            }
            if(!schema.getIndexes(Label.label("NodeDiff")).iterator().hasNext()) {
                IndexDefinition nodeDiffIndexDefinition = schema.indexFor(Label.label("NodeDiff")).on("id").create();
            }
            if(!schema.getIndexes(Label.label("EdgeDiff")).iterator().hasNext()) {
                IndexDefinition edgeDiffIndexDefinition = schema.indexFor(Label.label("EdgeDiff")).on("edgeID").create();
            }
            tx.success();
            tx.close();

            Set<String> virtualNodes = new HashSet<>();
            System.setOut(new PrintStream(new OutputStream() {
                @Override
                public void write(int arg0) throws IOException {}
            }));

            DirectoryStream<Path> stream = Files.newDirectoryStream(p, path -> Files.isRegularFile(path)
                    && Files.size(path) > 231);



            List<Path> diffs = new ArrayList<>();
            for(Path file : stream)
            {
                if(Files.isRegularFile(file) && file.toString().endsWith(".json"))
                {
                    diffs.add(file);
                }
            }
            Collections.sort(diffs, new Comparator<Path>()
            {
                @Override
                public int compare(Path p1, Path p2)
                {
                    try {
                        return Long.compare(Files.size(p1),Files.size(p2));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return 0;
                }
            });
            if(limit < 0)
            {
                limit = diffs.size();
            }
            NodeChangeHandler nh = new NodeChangeHandler();
            EdgeChangeHandler eh = new EdgeChangeHandler();

            for(int i = 0; i < limit; i++)
            {
                Path diff = diffs.get(i);
                JsonReader reader = new JsonReader(new FileReader(diff.toFile()));
                JsonObject obj = new JsonParser().parse(reader).getAsJsonObject();
                Transaction tx2 = db.beginTx();
                handleRemovals(obj, nh, eh);
                tx2.success();
                tx2.close();
            }
            for(int i = 0; i < limit; i++)
            {
                Path diff = diffs.get(i);
                JsonReader reader = new JsonReader(new FileReader(diff.toFile()));
                JsonObject obj = new JsonParser().parse(reader).getAsJsonObject();
                Transaction tx2 = db.beginTx();
                handleChanges(obj, nh, eh);
                tx2.success();
                tx2.close();
            }

            Transaction net = db.beginTx();
            nh.recordNetChange(db);
            eh.recordNetChange(db);
            net.success();
            net.close();

            System.out.println("===Clean Up===");
            Transaction cleanUp = db.beginTx();
            ResourceIterator<Node> ri = db.findNodes(Label.label("BaseFact"));
            while(ri.hasNext())
            {
                Node n = ri.next();
                if(!n.hasProperty("instances"))
                {
                    for(Relationship r : n.getRelationships(Direction.OUTGOING))
                    {
                        String edgeID = r.getType().name() + " " + n.getProperty("id") + " " + r.getEndNode().getProperty("id");
                        Node edgeDiff = db.findNode(Label.label("EdgeDiff"), "edgeID", edgeID);
                        if(edgeDiff != null)
                        {
                            System.out.println("Deleting Virtual Edge Diff: " + edgeID);
                            edgeDiff.delete();
                        }
                        System.out.println("Deleting Virtual Edge: " + edgeID);
                        r.delete();
                    }
                    for(Relationship r : n.getRelationships(Direction.INCOMING)) {
                        String edgeID = r.getType().name() + " " + r.getStartNode().getProperty("id") + " " + n.getProperty("id");
                        Node edgeDiff = db.findNode(Label.label("EdgeDiff"), "edgeID", edgeID);
                        if (edgeDiff != null) {
                            System.out.println("Deleting Virtual Edge Diff: " + edgeID);
                            edgeDiff.delete();
                        }
                        System.out.println("Deleting Virtual Edge: " + edgeID);
                        r.delete();
                    }
                    ResourceIterator<Node> nodeDiffs = db.findNodes(Label.label("NodeDiff"), "nodeID", n.getProperty("id"));
                    while(nodeDiffs.hasNext())
                    {
                        nodeDiffs.next().delete();
                    }
                    n.delete();
                }
            }
            cleanUp.success();
            cleanUp.close();

        }
        catch (Exception e)
        {
            e.printStackTrace();
            System.exit(1);
        }

    }


    private static void handleRemovals(JsonObject obj, NodeChangeHandler nh, EdgeChangeHandler eh)
    {
        // Edge Attr Removed
        eh.handleEntityAttrRemoved(obj.get("edgeAttrRemoved").getAsJsonArray(), db);

        // Edge Removed
        eh.handleEntityRemoved(obj.get("edgesRemoved").getAsJsonArray(), db);

        // Node Attr Removed
        nh.handleEntityAttrRemoved((JsonArray) obj.get("nodeAttrRemoved"), db);

        // Node Removed
        nh.handleEntityRemoved((JsonArray) obj.get("nodesRemoved"), db);
    }

    private static void handleChanges(JsonObject obj, NodeChangeHandler nh, EdgeChangeHandler eh) throws Exception
    {

        System.out.println("===Nodes Added===");
        // Nodes Added
        nh.handleEntityAdded((JsonArray) obj.get("nodesAdded"), db);

        System.out.println("===Node Attr Added===");
        // Node Attr Added
        nh.handleEntityAttrAdded((JsonArray) obj.get("nodeAttrAdded"), db);

        System.out.println("===Edges Added===");
        // Edge Added
        eh.handleEntityAdded(obj.get("edgesAdded").getAsJsonArray(), db);

        System.out.println("===Edge Attr Added===");
        // Edge Attr Added
        eh.handleEntityAttrAdded(obj.get("edgeAttrAdded").getAsJsonArray(), db);

        // Node Attr Change
        nh.handleEntityAttrChange((JsonArray) obj.get("nodeAttrChange"), db);

        // Edge Attr Change
        eh.handleEntityAttrChange(obj.get("edgeAttrChange").getAsJsonArray(), db);

    }



    private static void handleNodes(JsonObject obj, EntityChangeHandler nh) throws Exception {
        // Nodes Added
        nh.handleEntityAdded((JsonArray) obj.get("nodesAdded"), db);

        // Node Attr Added
        nh.handleEntityAttrAdded((JsonArray) obj.get("nodeAttrAdded"), db);

        // Node Attr Removed
        nh.handleEntityAttrRemoved((JsonArray) obj.get("nodeAttrRemoved"), db);

        // Node Removed
        nh.handleEntityRemoved((JsonArray) obj.get("nodesRemoved"), db);

        // Node Attr Change
        nh.handleEntityAttrChange((JsonArray) obj.get("nodeAttrChange"), db);

    }


    private static void handleEdges(JsonObject obj, EntityChangeHandler eh) throws Exception {
        // Edge Added
        eh.handleEntityAdded(obj.get("edgesAdded").getAsJsonArray(), db);

        // Edge Attr Added
        eh.handleEntityAttrAdded(obj.get("edgeAttrAdded").getAsJsonArray(), db);

        // Edge Attr Removed
        eh.handleEntityAttrRemoved(obj.get("edgeAttrRemoved").getAsJsonArray(), db);

        // Edge Removed
        eh.handleEntityRemoved(obj.get("edgesRemoved").getAsJsonArray(), db);

        // Edge Attr Change
        eh.handleEntityAttrChange(obj.get("edgeAttrChange").getAsJsonArray(), db);


    }
}
