package incremental;

import java.util.ArrayList;
import java.util.List;

public class JobSlot extends Thread
{
    private List<Runnable> jobs;

    public JobSlot()
    {
        super();
        jobs = new ArrayList<>();
    }

    public void addJob(Runnable job)
    {
        jobs.add(job);
    }

    @Override
    public void run()
    {
        for(Runnable job : jobs)
        {
            job.run();
        }
    }
}
