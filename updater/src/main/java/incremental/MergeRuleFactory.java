package incremental;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.neo4j.graphdb.Entity;
import org.neo4j.logging.Log;

import java.util.*;

public class MergeRuleFactory
{

    public static MergeRule<Entity, JsonObject> getMergeRule(String propertyName)
    {
        if(Attribute.isNumeric(propertyName))
        {
            return new MergeRule<Entity, JsonObject>()
            {

                @Override
                public void merge(Entity system, JsonObject local)
                {
                    int systemProp = ((Number) system.getProperty(propertyName)).intValue();
                    int localProp = local.get("val").getAsInt();

                    if(localProp == 1)
                    {
                        system.setProperty(propertyName, systemProp + 1);
                        //System.out.println("Merge!: " +  propertyName + "=" + (systemProp+1));
                    }
                }

                @Override
                public void unMerge(Entity system, JsonObject local) {

                    int systemProp = ((Number) system.getProperty(propertyName)).intValue();
                    int localProp = local.get("val").getAsInt();

                    if(localProp == 1)
                    {
                        system.setProperty(propertyName, systemProp - 1);
                        //System.out.println("Un-Merge!: " +  propertyName + "=" + (systemProp-1));
                    }
                }

                @Override
                public void reMerge(Entity system, JsonObject local) {
                    int systemProp = ((Number) system.getProperty(propertyName)).intValue();
                    int localProp = local.get("new_val").getAsInt();

                    if(localProp == 1)
                    {
                        system.setProperty(propertyName, systemProp + 1);
                        //System.out.println("Re-Merge!: +1 " +  propertyName);
                    }
                    else
                    {
                        system.setProperty(propertyName, systemProp - 1);
                        //System.out.println("Re-Merge!: -1 " +  propertyName);
                    }
                }
            };
        }

        //Now a multi-attribute
        if(propertyName.equals("condition"))
        {
            //TODO: Implement this using a Query and compare for large arrays
            return new MergeRule<Entity, JsonObject>() {
                @Override
                public void merge(Entity system, JsonObject local)
                {
                    JsonArray localCond = local.get("val").getAsJsonArray();
                    List<String> systemCond = new ArrayList<>(Arrays.asList((String[]) system.getProperty("condition")));
                    for(int i = 0; i < localCond.size(); i++)
                    {
                        String newCond = localCond.get(i).getAsString();
//                        if (!systemCond.contains(newCond)) {
                            systemCond.add(newCond);
                            system.setProperty(propertyName, systemCond.toArray(new String[0]));
//                        }
                    }
                    System.out.println("SystemCond: " + systemCond.toString());
                }

                @Override
                public void unMerge(Entity system, JsonObject local)
                {
                    JsonArray localCond = local.get("val").getAsJsonArray();
                    List<String> systemCond = new ArrayList<>(Arrays.asList((String[]) system.getProperty("condition")));
                    if(localCond.size() == systemCond.size())
                    {
                        system.removeProperty("condition");
                        System.out.println("Remove condition property");
                    }
                    else
                    {
                        for (int i = 0; i < localCond.size(); i++)
                        {
                            systemCond.remove(localCond.get(i).getAsString());
                        }
                        system.setProperty(propertyName, systemCond.toArray(new String[0]));
                        System.out.println("SystemCond: " + systemCond.toString());
                    }

                }

                @Override
                public void reMerge(Entity system, JsonObject local)
                {
                    JsonArray condsRemoved = local.get("vals_removed").getAsJsonArray();
                    JsonArray condsAdded = local.get("vals_added").getAsJsonArray();
                    List<String> systemCond = new ArrayList<>(Arrays.asList((String[]) system.getProperty("condition")));
                    for(int i = 0; i < condsRemoved.size(); i++)
                    {
                        systemCond.remove(condsRemoved.get(i).getAsString());
                    }
                    for(int i = 0; i < condsAdded.size(); i++)
                    {
                        systemCond.add(condsAdded.get(i).getAsString());
                    }
                    system.setProperty(propertyName, systemCond.toArray(new String[0]));
                    System.out.println("SystemCond: " + systemCond.toString());
                }
            };
        }

        return new MergeRule<Entity, JsonObject>() {
            @Override
            public void merge(Entity system, JsonObject local) {
                System.out.println("Merge rule undefined for property: " + propertyName);
            }

            @Override
            public void unMerge(Entity system, JsonObject local) {
                System.out.println("Un-merge rule undefined for property: " + propertyName);
            }

            @Override
            public void reMerge(Entity system, JsonObject local) {
                System.out.println("Re-merge rule undefined for property: " + propertyName);
            }
        };
    }
}
