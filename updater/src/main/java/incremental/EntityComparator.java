package incremental;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.neo4j.graphdb.Entity;
import java.util.Iterator;

public interface EntityComparator
{
    static boolean isEqual(Entity systemEntity, JsonObject localEntity)
    {
        Iterator<JsonElement> single_it = localEntity.getAsJsonArray("singleAttrs").iterator();
        while(single_it.hasNext())
        {
            JsonObject nextAttr = single_it.next().getAsJsonObject();
            if(!Attribute.isEqualSingleAttr(systemEntity, nextAttr))
            {
                return false;
            }
        }
        Iterator<JsonElement> multi_it = localEntity.getAsJsonArray("multiAttrs").iterator();
        while(multi_it.hasNext())
        {
            JsonObject nextAttr = multi_it.next().getAsJsonObject();
            if(!Attribute.isEqualMultiAttr(systemEntity, nextAttr))
            {
                return false;
            }
        }
        return true;

    }
}
