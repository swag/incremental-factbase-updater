package incremental;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import incremental.listeners.EdgeObserver;
import incremental.listeners.FactBaseSubject;
import incremental.listeners.NodeObserver;
import incremental.listeners.SubjectData;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.graphdb.*;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public abstract class EntityChangeHandler extends FactBaseSubject
{
    
    static final String ADD_OP = "Add";
    static final String REMOVE_OP = "Remove";
    static final String MOD_OP = "Mod";

    public EntityChangeHandler()
    {
        super();
        addObserver("node", new NodeObserver());
        addObserver("edge", new EdgeObserver());
    }

    private void recordNodeChange(GraphDatabaseService db)
    {
        NodeObserver no = (NodeObserver) getObserver("node");
        for(SubjectData data : no.events.values())
        {
            Map<String, Object> params = new HashMap<>();
            if(data.get("type").equals("remove"))
            {
                data.add("dirty", false);
                data.add("remove", true);
            }
            else
            {
                data.add("dirty", true);
                data.add("remove", false);
            }

            //Transaction tx = db.beginTx();
            Node n = db.findNode(Label.label("NodeDiff"), "id", data.get("id"));
            if(n == null)
            {
                n = db.createNode(Label.label("NodeDiff"));
            }
            if(data.get("type").equals("remove"))
            {
                Iterable<String> keys = n.getPropertyKeys();
                for(String key : keys)
                {
                    n.removeProperty(key);
                }
            }
            for(String key : data.getKeys())
            {
                if(!key.equals("type"))
                {
                    n.setProperty(key, data.get(key));
                }
            }

            //tx.success();
            //tx.close();
        }
    }

    private void recordNetEdgeChange(GraphDatabaseService db)
    {
        EdgeObserver eo = (EdgeObserver) getObserver("edge");
        for(SubjectData data : eo.events.values())
        {
            Map<String, Object> params = new HashMap<>();
            if(data.get("type").equals("remove"))
            {
                data.add("dirty", false);
                data.add("remove", true);
            }
            else
            {
                data.add("dirty", true);
                data.add("remove", false);
            }
            //Transaction tx = db.beginTx();
            Node n = db.findNode(Label.label("EdgeDiff"), "edgeID", data.get("edgeID"));
            if(n == null)
            {
                n = db.createNode(Label.label("EdgeDiff"));
            }
            if(data.get("type").equals("remove"))
            {
                Iterable<String> keys = n.getPropertyKeys();
                for(String key : keys)
                {
                    n.removeProperty(key);
                }
            }
            for(String key : data.getKeys())
            {
                if(!key.equals("type"))
                {
                    n.setProperty(key, data.get(key));
                }
            }

            //tx.success();
            //tx.close();
        }
    }

    public void recordNetChange(GraphDatabaseService db)
    {
        recordNodeChange(db);
        recordNetEdgeChange(db);
    }

    public abstract void handleEntityAdded(JsonArray arr, GraphDatabaseService db);
    public abstract void handleEntityAttrAdded(JsonArray arr, GraphDatabaseService db);
    public abstract void handleEntityRemoved(JsonArray arr, GraphDatabaseService db);
    public abstract void handleEntityAttrRemoved(JsonArray nodeAttrRemoved, GraphDatabaseService db);
    public abstract void handleEntityAttrChange(JsonArray nodeAttrChange, GraphDatabaseService db);

    public void applyEntityAttrAdded(Entity foundNode, JsonObject nodeAttr, SubjectData sd)
    {
        //Single Attrs
        Iterator single_it = (nodeAttr.get("singleAttrs").getAsJsonArray()).iterator();
        while(single_it.hasNext())
        {
            JsonObject singleAttr = (JsonObject) single_it.next();
            String attrName = singleAttr.get(Attribute.ATTR_NAME_KEY).getAsString();
            if(foundNode.hasProperty(attrName))
            {
                // Merge
                MergeRuleFactory.getMergeRule(attrName).merge(foundNode, singleAttr);
                System.out.println("Merge: " + attrName + "=" + foundNode.getProperty(attrName));
                sd.add(attrName, foundNode.getProperty(attrName));
            }
            else
            {
                if(!attrName.equals(Attribute.ATTR_CONDITION_NAME)) {
                    
                    if(Attribute.isNumeric(attrName))
                    {
                        foundNode.setProperty(attrName, singleAttr.get(Attribute.ATTR_VAL_KEY).getAsInt());
                        sd.add(attrName, singleAttr.get(Attribute.ATTR_VAL_KEY).getAsInt());
                    }
                    else
                    {
                        foundNode.setProperty(attrName, singleAttr.get(Attribute.ATTR_VAL_KEY).getAsString());
                        sd.add(attrName, singleAttr.get(Attribute.ATTR_VAL_KEY).getAsString());
                    }

                }
                else
                {
                    foundNode.setProperty(attrName, singleAttr.get(Attribute.ATTR_VAL_KEY).getAsString());
                    sd.add(attrName, singleAttr.get(Attribute.ATTR_VAL_KEY).getAsString());
                }
                System.out.println("Set: " + attrName + "=" + singleAttr.get(Attribute.ATTR_VAL_KEY).getAsString());
            }
        }
        //Multi Attrs
        Iterator<JsonElement> multi_it = ((JsonArray) nodeAttr.get("multiAttrs")).iterator();
        while(multi_it.hasNext())
        {
            JsonObject multiAttr =  multi_it.next().getAsJsonObject();
            String attrName =  multiAttr.get(Attribute.ATTR_NAME_KEY).getAsString();
            if(foundNode.hasProperty(attrName))
            {
                // Merge
                MergeRuleFactory.getMergeRule(attrName).merge(foundNode, multiAttr);
                sd.add(attrName, foundNode.getProperty(attrName));
            }
            else
            {
                JsonArray arr = multiAttr.get(Attribute.ATTR_VAL_KEY).getAsJsonArray();
                String[] newProp = new String[arr.size()];
                for(int i = 0; i< arr.size(); i++)
                {
                    newProp[i] = arr.get(i).getAsString();
                }
                foundNode.setProperty(attrName, newProp);
                sd.add(attrName, foundNode.getProperty(attrName));
            }
        }
    }

    public void applyEntityAttrRemoved(Entity foundNode, JsonObject nodeAttr, SubjectData sd)
    {
        //Single Attrs
        Iterator single_it = ((JsonArray) nodeAttr.get("singleAttrs")).iterator();
        while (single_it.hasNext()) {
            JsonObject singleAttr = (JsonObject) single_it.next();
            String attrName = singleAttr.get(Attribute.ATTR_NAME_KEY).getAsString();
            if (foundNode.hasProperty(attrName)) {
                //Un-merge
                MergeRuleFactory.getMergeRule(attrName).unMerge(foundNode, singleAttr);
                sd.add(attrName, foundNode.getProperty(attrName));
            }

        }
        //Multi Attrs
        Iterator multi_it = ((JsonArray) nodeAttr.get("multiAttrs")).iterator();
        while (multi_it.hasNext()) {
            JsonObject multiAttr = (JsonObject) multi_it.next();
            String attrName =  multiAttr.get(Attribute.ATTR_NAME_KEY).getAsString();
            if (foundNode.hasProperty(attrName)) {
                //Un-merge
                MergeRuleFactory.getMergeRule(attrName).unMerge(foundNode, multiAttr);
                sd.add(attrName, foundNode.getProperty(attrName));
            }
        }
    }


    public void applySingleAttrChange(JsonObject singleAttrs, Entity foundNode, SubjectData sd)
    {
        JsonArray added = (JsonArray) singleAttrs.get("Added");
        if(added != null)
        {
            Iterator added_it = added.iterator();
            while (added_it.hasNext()) {
                JsonObject singleAttr = (JsonObject) added_it.next();
                String attrName = singleAttr.get(Attribute.ATTR_NAME_KEY).getAsString();
                if (foundNode.hasProperty(attrName)) {
                    // Merge
                    System.out.println("Merge!");
                    MergeRuleFactory.getMergeRule(attrName).merge(foundNode, singleAttr);
                    sd.add(attrName, foundNode.getProperty(attrName));
                } else {
                    if(!attrName.equals(Attribute.ATTR_CONDITION_NAME)) {
                        if(Attribute.isNumeric(attrName))
                        {
                            foundNode.setProperty(attrName, singleAttr.get(Attribute.ATTR_VAL_KEY).getAsInt());
                            sd.add(attrName, singleAttr.get(Attribute.ATTR_VAL_KEY).getAsInt());
                        }
                        else
                        {
                            foundNode.setProperty(attrName, singleAttr.get(Attribute.ATTR_VAL_KEY).getAsString());
                            sd.add(attrName, singleAttr.get(Attribute.ATTR_VAL_KEY).getAsString());
                        }
                    }
                    else
                    {
                        foundNode.setProperty(attrName, singleAttr.get(Attribute.ATTR_VAL_KEY).getAsString());
                        sd.add(attrName, singleAttr.get(Attribute.ATTR_VAL_KEY).getAsString());
                    }
                    System.out.println("Set: " + attrName + "=" + singleAttr.get(Attribute.ATTR_VAL_KEY).getAsString());
                }
            }
        }
        JsonArray removed = (JsonArray) singleAttrs.get("Removed");
        if(removed != null)
        {
            Iterator removed_it = removed.iterator();
            while (removed_it.hasNext())
            {
                JsonObject singleAttr = (JsonObject) removed_it.next();
                String attrName = singleAttr.get(Attribute.ATTR_NAME_KEY).getAsString();
                if (foundNode.hasProperty(attrName))
                {
                    // Unmerge
                    MergeRuleFactory.getMergeRule(attrName).unMerge(foundNode, singleAttr);
                    sd.add(attrName, foundNode.getProperty(attrName, null));
                }
                else
                {
                    //foundNode.setProperty(attrName, singleAttr.get(Attribute.ATTR_VAL_KEY));
                }
            }
        }
        JsonArray modified = (JsonArray) singleAttrs.get("Modified");
        if(modified != null)
        {
            Iterator modified_it = modified.iterator();
            while (modified_it.hasNext()) {
                JsonObject singleAttr = (JsonObject) modified_it.next();
                String attrName = singleAttr.get(Attribute.ATTR_NAME_KEY).getAsString();
                if (foundNode.hasProperty(attrName))
                {
                    // Re-Merge
                    System.out.println("Re-Merge: " + attrName + "=" + foundNode.getProperty(attrName));
                    MergeRuleFactory.getMergeRule(attrName).reMerge(foundNode, singleAttr);
                    sd.add(attrName, foundNode.getProperty(attrName));
                }
                else
                {
                    //foundNode.setProperty(attrName, singleAttr.get(Attribute.ATTR_VAL_KEY));
                }
            }
        }
    }

    public void applyMultiAttrChange(JsonObject multiAttrs, Entity foundNode, SubjectData sd)
    {
        JsonArray added = (JsonArray) multiAttrs.get("Added");
        if(added != null)
        {
            Iterator added_it = added.iterator();
            while (added_it.hasNext())
            {
                JsonObject multiAttr = (JsonObject) added_it.next();
                String attrName = multiAttr.get(Attribute.ATTR_NAME_KEY).getAsString();
                if (foundNode.hasProperty(attrName))
                {
                    // Merge
                    MergeRuleFactory.getMergeRule(attrName).merge(foundNode, multiAttr);
                }
                else
                {
                    System.out.println("Set: " + attrName + "=" + multiAttr.get(Attribute.ATTR_VAL_KEY));
                    JsonArray arr = multiAttr.get(Attribute.ATTR_VAL_KEY).getAsJsonArray();
                    String[] newProp = new String[arr.size()];
                    for(int i = 0; i< arr.size(); i++)
                    {
                        newProp[i] = arr.get(i).getAsString();
                    }
                    foundNode.setProperty(attrName, newProp);
                }
                sd.add(attrName, foundNode.getProperty(attrName));
            }
        }
        JsonArray removed = (JsonArray) multiAttrs.get("Removed");
        if(removed != null)
        {
            Iterator removed_it = removed.iterator();
            while (removed_it.hasNext()) {
                JsonObject multiAttr = (JsonObject) removed_it.next();
                String attrName = multiAttr.get(Attribute.ATTR_NAME_KEY).getAsString();
                if (foundNode.hasProperty(attrName))
                {
                    // Un-Merge
                    MergeRuleFactory.getMergeRule(attrName).unMerge(foundNode, multiAttr);
                    sd.add(attrName, foundNode.getProperty(attrName, null));
                }
                else
                {
                    //foundNode.setProperty(attrName, singleAttr.get(Attribute.ATTR_VAL_KEY));
                }
            }
        }
        JsonArray modified = (JsonArray) multiAttrs.get("Modified");
        if(modified != null)
        {
            Iterator modified_it = modified.iterator();
            while (modified_it.hasNext()) {
                JsonObject multiAttr = (JsonObject) modified_it.next();
                String attrName = multiAttr.get(Attribute.ATTR_NAME_KEY).getAsString();
                if (foundNode.hasProperty(attrName))
                {
                    // Re-Merge
                    MergeRuleFactory.getMergeRule(attrName).reMerge(foundNode, multiAttr);
                    sd.add(attrName, foundNode.getProperty(attrName));
                }
                else
                {
                    //foundNode.setProperty(attrName, singleAttr.get(Attribute.ATTR_VAL_KEY));
                }
            }
        }
    }



}
