package incremental;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Result;

import java.util.ArrayList;

public class CypherLayer
{
    static Label getNodeLabel(String id, GraphDatabaseService db)
    {
        Result r = db.execute("Match (n {id:\'" + id + "\'}) Return labels(n) as label");
        if(r.hasNext())
        {
            ArrayList<Object> labels = (ArrayList<Object>) r.next().get("label");
            String labelString = (String) labels.get(0);
            System.out.println(labelString);
            return Label.label(labelString);
        }
        return Label.label("");
    }
}
