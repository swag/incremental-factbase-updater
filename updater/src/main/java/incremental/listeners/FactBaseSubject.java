package incremental.listeners;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FactBaseSubject
{
    private Map<String, FactBaseObserver> observers;

    public FactBaseSubject()
    {
        observers = new HashMap<>();
    }

    public void addObserver(String key, FactBaseObserver a_observer)
    {
        observers.put(key, a_observer);
    }

    public FactBaseObserver getObserver(String key)
    {
        return observers.get(key);
    }

    public void removeObserver(String key, FactBaseObserver a_observer)
    {
        observers.remove(key);
    }

    public void notifyObserver(String key, SubjectData data)
    {
        observers.get(key).update(data);
    }

}
