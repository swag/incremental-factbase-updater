package incremental.listeners;

public interface FactBaseObserver
{
    void update(SubjectData data);

}
