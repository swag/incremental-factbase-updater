package incremental.listeners;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Label;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;

import java.util.HashMap;
import java.util.Map;

/**
 * Record changes that are made to the extracted factbase
 * during an incremental update
 *
 * Each change shall have one and only one type:
 * 1) Addition
 * 2) Modification
 * 3) Removal
 */
public class EdgeObserver implements FactBaseObserver
{
    public Map<String, SubjectData> events;
    public EdgeObserver()
    {
        super();
        events = new HashMap<>();
    }


    @Override
    public void update(SubjectData data)
    {
        String srcID = (String) data.get("src");
        String dstID = (String) data.get("dst");
        String edgeType = (String) data.get("edgeType");

        String edgeID = edgeType + " " + srcID + " " + dstID;
        data.add("edgeID", edgeID);
        if(!events.containsKey(edgeID))
        {
            events.put(edgeID, data);
        }
        else
        {
            if(data.get("type").equals("remove"))
            {
                events.replace(edgeID, data);
            }
            else
            {
                data.remove("type");
                data.remove("src");
                data.remove("dst");
                data.remove("edgeType");
                data.remove("edgeID");
                events.get(edgeID).setAll(data);
            }
        }


    }
}
