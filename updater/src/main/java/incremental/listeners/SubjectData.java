package incremental.listeners;

import java.util.HashMap;
import java.util.Map;

public class SubjectData
{
    private Map<String, Object> data;

    public SubjectData()
    {
        data = new HashMap<>();
    }

    public void add(String key, Object newData)
    {
        if(!data.containsKey(key))
        {
            data.put(key, newData);
        }
    }


    public Map<String, Object> getData()
    {
        return data;
    }

    public Iterable<String> getKeys()
    {
        return data.keySet();
    }

    public void setAll(SubjectData s)
    {
        data.putAll(s.getData());
    }

    public void set(String key, Object newData)
    {
        data.put(key, newData);
    }

    public void remove(String key)
    {
        data.remove(key);
    }

    public Object get(String key)
    {
        return data.get(key);
    }

    @Override
    public boolean equals(Object o)
    {
        if(o instanceof SubjectData)
        {
            SubjectData other = (SubjectData) o;
            return data.equals(other.data);
        }
        return false;
    }

}
