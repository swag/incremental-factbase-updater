package incremental.listeners;

import org.neo4j.cypher.internal.v3_4.functions.Rand;
import org.neo4j.graphdb.*;

import java.util.*;

/**
 * Record changes that are made to the extracted factbase
 * during an incremental update
 *
 * Each change shall have one and only one type:
 * 1) Addition
 * 2) Modification
 * 3) Removal
 */
public class NodeObserver implements FactBaseObserver
{
    public Map<String, SubjectData> events;

    public NodeObserver()
    {
        super();
        events = new HashMap<>();
    }


    @Override
    public synchronized void update(SubjectData data)
    {
        String id =  data.get("nodeID") + " " + data.get("nodeType");
        data.add("id", id);
        if(!events.containsKey(id))
        {
            events.put(id, data);
        }
        else
        {
            if(data.get("type").equals("remove"))
            {
                events.replace(id, data);
            }
            else
            {
                events.get(id).setAll(data);
            }
        }

    }

}
