package incremental;


import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.neo4j.graphdb.Entity;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Attribute
{
    public static final Set<String> booleanProps = new HashSet<>(Arrays.asList
            ("isControlFlow", "isCallbackFunc", "isParam", "isOneshot"));


    public static final String ATTR_NAME_KEY = "name";
    public static final String ATTR_VAL_KEY = "val";
    public static final String ATTR_CONDITION_NAME = "condition";


    public static boolean isNumeric(String attrName)
    {
        return booleanProps.contains(attrName) || attrName.equals("instances");
    }

    /**
     * A system fact and a local fact are equal iff
     * the local fact's properties are equal to
     * the system fact's properties
     * @param systemEntity
     * @param localEntity
     * @return
     */
    public static boolean isEqualSingleAttr(Entity systemEntity, JsonObject localEntity)
    {
        String attrName = localEntity.get(ATTR_NAME_KEY).getAsString();
        if(systemEntity.hasProperty(attrName))
        {
            if(isNumeric(attrName))
            {
                int val1 = localEntity.get(ATTR_VAL_KEY).getAsInt();
                int val2 = ((Number) systemEntity.getProperty(attrName)).intValue();
                return val1 == val2;
            }
            else
            {
                String val1 = localEntity.get(ATTR_VAL_KEY).getAsString();
                String val2 = (String) systemEntity.getProperty(attrName);
                return val1.equals(val2);
            }
        }
        return false;
    }
    /**
     * A system fact and a local fact are equal iff
     * the local fact's properties are equal to
     * the system fact's properties
     * @param systemEntity
     * @param localEntity
     * @return
     */
    public static boolean isEqualMultiAttr(Entity systemEntity, JsonObject localEntity)
    {
        String attrName = localEntity.get(ATTR_NAME_KEY).getAsString();
        if(systemEntity.hasProperty(attrName))
        {
            JsonArray arr = localEntity.get(ATTR_VAL_KEY).getAsJsonArray();
            String[] val1 = new String[arr.size()];
            for(int i = 0; i < arr.size(); i++)
            {
                val1[i] = arr.get(i).getAsString();
            }
            String[] val2 = (String[]) systemEntity.getProperty(attrName);
            return Arrays.equals(val1, val2);
        }
        return false;
    }
}
